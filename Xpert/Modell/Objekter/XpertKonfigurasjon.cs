﻿using System;
using System.IO;

namespace Xpert.Modell.Objekter
{
    public static class XpertKonfigurasjon
    {
        public static string KatalogXpert
        {
            get
            {
                return Directory.GetCurrentDirectory();
            }
        }

        public static string KatalogXpertOnline
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Xpert\\Online");
            }
        }

        public static string KatalogXpertData
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Xpert\\Data");
            }
        }

        public static string KatalogNTF
        {
            get
            {
                return "A:\\NTFILE";
            }
        }

        public static string KatalogDesktop
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            }
        }
    }
}
