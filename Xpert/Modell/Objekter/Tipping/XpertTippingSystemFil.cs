﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Xpert.Modell.Objekter.Tipping
{
    public static class XpertTippingSystemFil
    {
        private static string[,] DekodLagData(byte[] data)
        {
            int nLag =0, startIndeks = -1;
            bool bHjemme = true;
            string[,] lagData = new string[12, 2];
            for (int n = 0; n < data.Length; n++)
            {
                if (data[n] == 0)
                {
                    if (bHjemme)
                    {
                        if ((n - startIndeks) > 1)
                            lagData[nLag, 0] = Encoding.UTF8.GetString(data, startIndeks + 1, n - (startIndeks + 1));
                        else
                            lagData[nLag, 0] = String.Empty;
                        bHjemme = false;
                    }
                    else
                    {
                        if ((n - startIndeks) > 1)
                            lagData[nLag, 1] = Encoding.UTF8.GetString(data, startIndeks + 1, n - (startIndeks + 1));
                        else
                            lagData[nLag, 1] = String.Empty;
                        bHjemme = true;
                        nLag++;
                    }
                    startIndeks = n;
                }
            }
            return lagData;
        }

        private static int KodLagData(string[,] lag, out byte[] lagData)
        {
            List<byte> _lagData = new List<byte>();
            for (int nLag = 0; nLag < 12; nLag++)
            {
                if (!String.IsNullOrEmpty(lag[nLag, 0]))
                    _lagData.AddRange(Encoding.UTF8.GetBytes(lag[nLag, 0]));
                _lagData.Add(0);
                if (!String.IsNullOrEmpty(lag[nLag, 1]))
                    _lagData.AddRange(Encoding.UTF8.GetBytes(lag[nLag, 1]));
                _lagData.Add(0);
            }
            lagData = _lagData.ToArray();
            return lagData.Length;
        }

        public static XpertTippingSystem Hent(string filnavn)
        {
            XpertTippingSystem system = new XpertTippingSystem();
            using (FileStream fs = File.Open(filnavn, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                byte[] signatur_bytes = new byte[Encoding.ASCII.GetBytes("XPERT10").Length];
                fs.Read(signatur_bytes, 0, signatur_bytes.Length);
                string signatur = Encoding.ASCII.GetString(signatur_bytes);
                if (signatur != "XPERT10")
                    throw new XpertException("Ukjent signatur !");
                byte[] lagDataLen_bytes = new byte[sizeof(int)];
                fs.Read(lagDataLen_bytes, 0, lagDataLen_bytes.Length);
                int lagDataLen = BitConverter.ToInt32(lagDataLen_bytes, 0);
                int antBlokker = fs.ReadByte();
                int antUtganger = fs.ReadByte();
                int antTegnfordelinger = fs.ReadByte();
                int antMaksRad = fs.ReadByte();

                byte[] lagData = new byte[lagDataLen];
                fs.Read(lagData, 0, lagDataLen);
                system.Lag = DekodLagData(lagData);
                fs.Read(system.Stamme.Markering, 0, system.Stamme.Markering.Length);

                for (int nBlokk = 0; nBlokk < antBlokker; nBlokk++)
                {
                    XpertTippingBlokk blokk = new XpertTippingBlokk();
                    fs.Read(blokk.Markering, 0, blokk.Markering.Length);
                    system.Blokker.Add(blokk);
                }

                for (int nUtgang = 0; nUtgang < antUtganger; nUtgang++)
                {
                    XpertTippingUtgang utgang = new XpertTippingUtgang();
                    fs.Read(utgang.Markering, 0, utgang.Markering.Length);
                    for (int nMarkeringTillatt = 0; nMarkeringTillatt < utgang.MarkeringerTillatt.Length; nMarkeringTillatt++)
                    {
                        int markeringTillatt = fs.ReadByte();
                        utgang.MarkeringerTillatt[nMarkeringTillatt] = (markeringTillatt == 0) ? false : true;
                    }
                    system.Utganger.Add(utgang);
                }

                for (int nTegnFordeling = 0; nTegnFordeling < antTegnfordelinger; nTegnFordeling++)
                {
                    XpertTippingFordeling fordeling = new XpertTippingFordeling();
                    int invertert = fs.ReadByte();
                    fordeling.Invertert = (invertert > 0) ? true : false;
                    for (int nMarkert = 0; nMarkert < 12; nMarkert++)
                    {
                        int markert = fs.ReadByte();
                        fordeling.Markert[nMarkert] = (markert > 0) ? true : false;
                    }
                    for (int nAntTillatt = 0; nAntTillatt < 13; nAntTillatt++)
                    {
                        int tillattH = fs.ReadByte();
                        fordeling.AntH[nAntTillatt] = (tillattH > 0) ? true : false;
                        int tillattU = fs.ReadByte();
                        fordeling.AntU[nAntTillatt] = (tillattU > 0) ? true : false;
                        int tillattB = fs.ReadByte();
                        fordeling.AntB[nAntTillatt] = (tillattB > 0) ? true : false;
                    }
                    system.TegnFordelinger.Add(fordeling);
                }

                if (antMaksRad != 0)
                {
                    XpertTippingMaksRad maksRad = new XpertTippingMaksRad();
                    maksRad.AntH = (byte)fs.ReadByte();
                    maksRad.AntU = (byte)fs.ReadByte();
                    maksRad.AntB = (byte)fs.ReadByte();
                    system.MaksRad = maksRad;
                }
            }
            return system;
        }

        public static void Lagre(string filnavn, XpertTippingSystem system)
        {
            if ((!String.IsNullOrEmpty(filnavn)) && (filnavn.EndsWith(".xpf")))
            {
                string dirPath = Path.GetDirectoryName(filnavn);
                if ((!Directory.Exists(dirPath)) && (!String.IsNullOrEmpty(dirPath)))
                    Directory.CreateDirectory(dirPath);

                using (FileStream fs = File.Create(filnavn, 1024))
                {
                    byte[] signatur = Encoding.ASCII.GetBytes("XPERT10");
                    fs.Write(signatur, 0, signatur.Length);
                    byte[] lagData;
                    int lagDataLen = KodLagData(system.Lag, out lagData);
                    byte[] intBytes = BitConverter.GetBytes(lagDataLen);
                    fs.Write(intBytes, 0, intBytes.Length);
                    fs.WriteByte((byte)system.Blokker.Count);
                    fs.WriteByte((byte)system.Utganger.Count);
                    fs.WriteByte((byte)system.TegnFordelinger.Count);
                    fs.WriteByte((byte)((system.MaksRad != null) ? 1 : 0));

                    fs.Write(lagData, 0, lagData.Length);
                    fs.Write(system.Stamme.Markering, 0, system.Stamme.Markering.Length);

                    for (int nBlokk = 0; nBlokk < system.Blokker.Count; nBlokk++)
                    {
                        fs.Write(system.Blokker[nBlokk].Markering, 0, system.Blokker[nBlokk].Markering.Length);
                    }

                    for (int nUtgang = 0; nUtgang < system.Utganger.Count; nUtgang++)
                    {
                        fs.Write(system.Utganger[nUtgang].Markering, 0, system.Utganger[nUtgang].Markering.Length);
                        for (int nMarkeringTillatt = 0; nMarkeringTillatt < system.Utganger[nUtgang].MarkeringerTillatt.Length; nMarkeringTillatt++)
                            fs.WriteByte((byte)((system.Utganger[nUtgang].MarkeringerTillatt[nMarkeringTillatt]) ? 1 : 0));
                    }

                    for (int nTegnFordeling = 0; nTegnFordeling < system.TegnFordelinger.Count; nTegnFordeling++)
                    {
                        fs.WriteByte((byte)((system.TegnFordelinger[nTegnFordeling].Invertert) ? 1 : 0));
                        for (int nMarkert = 0; nMarkert < 12; nMarkert++)
                            fs.WriteByte((byte)((system.TegnFordelinger[nTegnFordeling].Markert[nMarkert]) ? 1 : 0));
                        for (int nAntTillatt = 0; nAntTillatt < 13; nAntTillatt++)
                        {
                            fs.WriteByte((byte)((system.TegnFordelinger[nTegnFordeling].AntH[nAntTillatt]) ? 1 : 0));
                            fs.WriteByte((byte)((system.TegnFordelinger[nTegnFordeling].AntU[nAntTillatt]) ? 1 : 0));
                            fs.WriteByte((byte)((system.TegnFordelinger[nTegnFordeling].AntB[nAntTillatt]) ? 1 : 0));
                        }
                    }

                    if (system.MaksRad != null)
                    {
                        fs.WriteByte((byte)system.MaksRad.AntH);
                        fs.WriteByte((byte)system.MaksRad.AntU);
                        fs.WriteByte((byte)system.MaksRad.AntB);
                    }
                }
            }
        }
    }
}
