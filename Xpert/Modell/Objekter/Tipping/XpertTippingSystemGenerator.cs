﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Xpert.Modell.Objekter.Tipping
{
    public static class XpertTippingSystemGenerator
    {
        public struct ReduserTegn
        {
            public int Kamp;
            public byte[] Tegn;
        }

        static byte[,] GenererBlokk(byte[] t, out int nblokk)
        {
            byte[,] temp = new byte[27, 3];
            int n = 0;
            for (byte n0 = 0x04; n0 >= 0x01; n0 >>= 1)
            {
                for (byte n1 = 0x04; n1 >= 0x01; n1 >>= 1)
                {
                    for (byte n2 = 0x04; n2 >= 0x01; n2 >>= 1)
                    {
                        int n4 = n0 + (n1 << 3) + (n2 << 6);
                        int t4 = t[0] + (t[1] << 3) + (t[2] << 6);
                        if ((n4 & t4) == n4)
                        {
                            temp[n, 0] = n0;
                            temp[n, 1] = n1;
                            temp[n, 2] = n2;
                            n++;
                        }
                    }
                }
            }
            nblokk = n;
            byte[,] blokk = new byte[n, 3];
            Array.Copy(temp, blokk, blokk.Length);
            return blokk;
        }

        static bool ForkastRekke(byte[] markeringer, XpertTippingUtganger utganger)
        {
            for (int nUtgang = 0; nUtgang < utganger.Count; nUtgang++)
            {
                int antUtgang = 0;
                XpertTippingUtgang utgang = utganger[nUtgang];
                for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
                {
                    if (utgang.Markering[nMarkering].Equals(markeringer[nMarkering]))
                        antUtgang++;
                }
                if (!utgang.MarkeringerTillatt[antUtgang]) return true;
            }
            return false;
        }

        static bool ForkastRekke(byte[] markeringer, XpertTippingFordelinger tegnFordelinger)
        {
            for (int nTegnFordeling = 0; nTegnFordeling < tegnFordelinger.Count; nTegnFordeling++)
            {
                int antH = 0, antU = 0, antB = 0;
                for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
                {
                    if (tegnFordelinger[nTegnFordeling].Markert[nMarkering])
                    {
                        switch (markeringer[nMarkering])
                        {
                            case 1: antB++; break;
                            case 2: antU++; break;
                            case 4: antH++; break;
                        }
                    }
                }
                if (!tegnFordelinger[nTegnFordeling].AntH[antH]) return true;
                if (!tegnFordelinger[nTegnFordeling].AntU[antU]) return true;
                if (!tegnFordelinger[nTegnFordeling].AntB[antB]) return true;
            }
            return false;
        }

        static bool ForkastRekke(byte[] markeringer, XpertTippingMaksRad maksRad)
        {
            int antH = 0, antU = 0, antB = 0;
            int antHM = 1, antUM = 1, antBM = 1;
            for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
            {
                if (markeringer[nMarkering].Equals(1))
                {
                    antH = 0;
                    antU = 0;
                    antB++;
                }
                else if (markeringer[nMarkering].Equals(2))
                {
                    antH = 0;
                    antU++;
                    antB = 0;
                }
                else if (markeringer[nMarkering].Equals(4))
                {
                    antH++;
                    antU = 0;
                    antB = 0;
                }
                antHM = (antHM < antH) ? antH : antHM;
                antUM = (antUM < antU) ? antU : antUM;
                antBM = (antBM < antB) ? antB : antBM;
            }
            return (antHM > maksRad.AntH) ? true :
                    (antUM > maksRad.AntU) ? true :
                    (antBM > maksRad.AntB) ? true : false;
        }

        static bool ForkastRekke(byte[] markeringer, ReduserTegn[] reduserTegn)
        {
            for (int nReduserTegn = 0; nReduserTegn < reduserTegn.Length; nReduserTegn++)
            {
                if (markeringer[reduserTegn[nReduserTegn].Kamp] != reduserTegn[nReduserTegn].Tegn[0])
                    return true;
            }
            return false;
        }

        static bool ForkastRekke(byte[] markeringer, XpertTippingBlokker blokker)
        {
            for (int n = 0; n < 4; n++)
            {
                int nStartMarkering = n * 3;
                bool blokkOk = true;
                for (int nBlokk = 0; nBlokk < blokker.Count; nBlokk++)
                {
                    blokkOk = true;
                    for (int nMarkering = nStartMarkering; nMarkering < nStartMarkering + 3; nMarkering++)
                    {
                        if ((markeringer[nMarkering] & blokker[nBlokk].Markering[nMarkering]) != markeringer[nMarkering])
                        {
                            if (blokkOk) blokkOk = false;
                        }
                    }
                    if (blokkOk) break;
                }

                if (!blokkOk) return true;
            }
            return false;
        }

        static int[] FinnHelGarderinger(byte[] stamme)
        {
            List<int> markeringer = new List<int>();
            for (int nMarkering = 0; nMarkering < stamme.Length; nMarkering++)
            {
                if (stamme[nMarkering].Equals(7)) markeringer.Add(nMarkering);
            }
            return markeringer.ToArray();
        }

        static int[] FinnHalvGarderinger(byte[] stamme)
        {
            List<int> markeringer = new List<int>();
            for (int nMarkering = 0; nMarkering < stamme.Length; nMarkering++)
            {
                switch (stamme[nMarkering])
                {
                    case 3: markeringer.Add(nMarkering);
                        break;
                    case 5: markeringer.Add(nMarkering);
                        break;
                    case 6: markeringer.Add(nMarkering);
                        break;
                }
            }
            return markeringer.ToArray();
        }

        static byte[] FinnEnkeltTegn(byte gardering)
        {
            byte[] tegn = new byte[2];
            switch (gardering)
            {
                case 3:
                    tegn[0] = 2;
                    tegn[1] = 1;
                    break;
                case 5:
                    tegn[0] = 4;
                    tegn[1] = 1;
                    break;
                case 6:
                    tegn[0] = 4;
                    tegn[1] = 2;
                    break;
                case 7:
                    tegn = new byte[3];
                    tegn[0] = 4;
                    tegn[1] = 2;
                    tegn[2] = 1;
                    break;
            }
            return tegn;
        }

        static ReduserTegn[] FinnReduserTegn(byte[] stamme, int garantiAnt)
        {
            List<ReduserTegn> reduserTegn = new List<ReduserTegn>();
            int[] helmark = FinnHelGarderinger(stamme);
            for (int nMarkering = 0; nMarkering < helmark.Length; nMarkering++)
            {
                ReduserTegn markering = new ReduserTegn();
                markering.Kamp = helmark[nMarkering];
                markering.Tegn = FinnEnkeltTegn(stamme[helmark[nMarkering]]);
                reduserTegn.Add(markering);
            }
            int[] halvmark = FinnHalvGarderinger(stamme);
            for (int nMarkering = 0; nMarkering < halvmark.Length; nMarkering++)
            {
                ReduserTegn markering = new ReduserTegn();
                markering.Kamp = halvmark[nMarkering];
                markering.Tegn = FinnEnkeltTegn(stamme[halvmark[nMarkering]]);
                reduserTegn.Add(markering);
            }

            switch (garantiAnt)
            {
                case 9: while (reduserTegn.Count > 3) reduserTegn.RemoveAt(reduserTegn.Count - 1);
                    break;
                case 10: while (reduserTegn.Count > 2) reduserTegn.RemoveAt(reduserTegn.Count - 1);
                    break;
                case 11: while (reduserTegn.Count > 1) reduserTegn.RemoveAt(reduserTegn.Count - 1);
                    break;
            }
            return reduserTegn.ToArray();
        }

        public static XpertTippingRekker Generer(XpertTippingSystem system)
        {
            byte[] stamme = system.Stamme.Markering;

            byte[] tegn = new byte[] { 
                stamme[0], stamme[1], stamme[2], stamme[3], 
                stamme[4], stamme[5], stamme[6], stamme[7], 
                stamme[8], stamme[9], stamme[10], stamme[11] };

            int nblokk1;
            byte[,] blokk1 = GenererBlokk(new byte[] { tegn[0], tegn[1], tegn[2] }, out nblokk1);
            int nblokk2;
            byte[,] blokk2 = GenererBlokk(new byte[] { tegn[3], tegn[4], tegn[5] }, out nblokk2);
            int nblokk3;
            byte[,] blokk3 = GenererBlokk(new byte[] { tegn[6], tegn[7], tegn[8] }, out nblokk3);
            int nblokk4;
            byte[,] blokk4 = GenererBlokk(new byte[] { tegn[9], tegn[10], tegn[11] }, out nblokk4);

            XpertTippingRekker rekker = new XpertTippingRekker();
            for (int nblk1 = 0; nblk1 < nblokk1; nblk1++)
            {
                for (int nblk2 = 0; nblk2 < nblokk2; nblk2++)
                {
                    for (int nblk3 = 0; nblk3 < nblokk3; nblk3++)
                    {
                        for (int nblk4 = 0; nblk4 < nblokk4; nblk4++)
                        {
                            XpertTippingRekke rekke = new XpertTippingRekke();
                            rekke.Markering[0] = blokk1[nblk1, 0];
                            rekke.Markering[1] = blokk1[nblk1, 1];
                            rekke.Markering[2] = blokk1[nblk1, 2];

                            rekke.Markering[3] = blokk2[nblk2, 0];
                            rekke.Markering[4] = blokk2[nblk2, 1];
                            rekke.Markering[5] = blokk2[nblk2, 2];

                            rekke.Markering[6] = blokk3[nblk3, 0];
                            rekke.Markering[7] = blokk3[nblk3, 1];
                            rekke.Markering[8] = blokk3[nblk3, 2];

                            rekke.Markering[9] = blokk4[nblk4, 0];
                            rekke.Markering[10] = blokk4[nblk4, 1];
                            rekke.Markering[11] = blokk4[nblk4, 2];

                            bool bForkast = false;
                            if (system.Blokker.Count > 0)
                            {
                                bForkast = ForkastRekke(rekke.Markering, system.Blokker);
                                if (bForkast) continue;
                            }

                            if (system.Utganger.Count > 0)
                            {
                                bForkast = ForkastRekke(rekke.Markering, system.Utganger);
                                if (bForkast) continue;
                            }

                            if (system.TegnFordelinger.Count > 0)
                            {
                                bForkast = ForkastRekke(rekke.Markering, system.TegnFordelinger);
                                if (bForkast) continue;
                            }

                            if (system.MaksRad != null)
                            {
                                bForkast = ForkastRekke(rekke.Markering, system.MaksRad);
                                if (bForkast) continue;
                            }
                            rekker.Add(rekke);
                        }
                    }
                }
            }
            return rekker;
        }
    }
}
