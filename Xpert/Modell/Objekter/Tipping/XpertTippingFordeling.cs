﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingFordeling
    {
        private bool invertert = false;
        private bool[] markert = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
        private bool[] antH = new bool[13] { false, false, false, false, false, false, false, false, false, false, false, false, false };
        private bool[] antU = new bool[13] { false, false, false, false, false, false, false, false, false, false, false, false, false };
        private bool[] antB = new bool[13] { false, false, false, false, false, false, false, false, false, false, false, false, false };

        public bool Invertert
        {
            get
            {
                return invertert;
            }
            set
            {
                invertert = value;
            }
        }

        public bool[] Markert
        {
            get
            {
                return markert;
            }
            set
            {
                markert = value;
            }
        }

        public bool[] AntH
        {
            get
            {
                return antH;
            }
            set
            {
                antH = value;
            }
        }

        public bool[] AntU
        {
            get
            {
                return antU;
            }
            set
            {
                antU = value;
            }
        }

        public bool[] AntB
        {
            get
            {
                return antB;
            }
            set
            {
                antB = value;
            }
        }
    }
}
