﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingBlokk
    {
        private byte[] blokker = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        public byte[] Markering
        {
            get
            {
                return blokker;
            }
            set
            {
                blokker = value;
            }
        }
    }
}
