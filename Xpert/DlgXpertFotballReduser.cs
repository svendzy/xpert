﻿using System;
using System.Windows.Forms;

namespace Xpert
{
    public partial class DlgTipperuReduser : Form
    {
        private int garantiAnt;
        private bool garantiUtfyllingTilfeldig;

        public DlgTipperuReduser()
        {
            InitializeComponent();
        }

        public int GarantiAnt
        {
            get
            {
                return garantiAnt;
            }
            set
            {
                SettGarantiAnt(value);
            }
        }

        public bool GarantiUtfyllingTilfeldig
        {
            get
            {
                return garantiUtfyllingTilfeldig;
            }
            set
            {
                SettGarantiUtfyllingTilfeldig(value);
            }
        }

        private void SettGarantiAnt(int value)
        {
            switch (value)
            {
                case 9: 
                    rbGaranti9Rette.Checked = true;
                    garantiAnt = 9;
                    break;
                case 10: rbGaranti10Rette.Checked = true;
                    garantiAnt = 10;
                    break;
                case 11: rbGaranti11Rette.Checked = true;
                    garantiAnt = 11;
                    break;
                case 12: rbGarantiAlltid12rette.Checked = true;
                    garantiAnt = 12;
                    break;
            }
        }

        private void SettGarantiUtfyllingTilfeldig(bool value)
        {
            switch (value)
            {
                case false: 
                    garantiUtfyllingTilfeldig = false;
                    break;
                case true: 
                    garantiUtfyllingTilfeldig = true;
                    break;
            }
        }

        private void rbGaranti9Rette_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGaranti9Rette.Checked)
            {
                garantiAnt = 9;
            }
        }

        private void rbGaranti10Rette_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGaranti10Rette.Checked)
            {
                garantiAnt = 10;
            }
        }

        private void rbGaranti11Rette_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGaranti11Rette.Checked)
            {
                garantiAnt = 11;
            }
        }

        private void rbGarantiAlltid12rette_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGarantiAlltid12rette.Checked)
            {
                garantiAnt = 12;
            }
        }
    }
}
