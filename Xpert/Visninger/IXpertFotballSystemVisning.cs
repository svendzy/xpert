﻿using System;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Visninger
{
    public interface IXpertFotballSystemVisning : IXpertVisning
    {
        string SystemFil { get; set; }
        XpertTippingSystem XpertSystem { set; }
        XpertTippingRekker Rekker { set; }
    }
}
