﻿using System;
using System.Collections;
using System.Collections.Generic;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Visninger
{
    public interface IXpertFotballLaginfoVisning : IXpertVisning
    {
        Dictionary<string, ArrayList> LagInfo { set; }
    }
}
