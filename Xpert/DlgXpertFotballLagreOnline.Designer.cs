﻿namespace Xpert
{
    partial class DlgXpertFotballLagreOnline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgXpertFotballLagreOnline));
            this.txtSpillerNummer = new System.Windows.Forms.TextBox();
            this.btnLagre = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSpilletidBegge = new System.Windows.Forms.RadioButton();
            this.rbSpilletidHalv = new System.Windows.Forms.RadioButton();
            this.rbSpilletidFull = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbTrekningTypeMidtuke = new System.Windows.Forms.RadioButton();
            this.rbTrekningTypeSøndag = new System.Windows.Forms.RadioButton();
            this.rbTrekningTypeLørdag = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSpillerId = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnVelgKatalog = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilnavn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblKatalog = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSpillerNummer
            // 
            this.txtSpillerNummer.Location = new System.Drawing.Point(84, 28);
            this.txtSpillerNummer.MaxLength = 9;
            this.txtSpillerNummer.Name = "txtSpillerNummer";
            this.txtSpillerNummer.Size = new System.Drawing.Size(76, 20);
            this.txtSpillerNummer.TabIndex = 8;
            this.txtSpillerNummer.Text = "000000000";
            // 
            // btnLagre
            // 
            this.btnLagre.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLagre.Location = new System.Drawing.Point(182, 316);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 11;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbSpilletidBegge);
            this.groupBox1.Controls.Add(this.rbSpilletidHalv);
            this.groupBox1.Controls.Add(this.rbSpilletidFull);
            this.groupBox1.Location = new System.Drawing.Point(12, 159);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(245, 51);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spilletid";
            // 
            // rbSpilletidBegge
            // 
            this.rbSpilletidBegge.AutoSize = true;
            this.rbSpilletidBegge.Location = new System.Drawing.Point(163, 19);
            this.rbSpilletidBegge.Name = "rbSpilletidBegge";
            this.rbSpilletidBegge.Size = new System.Drawing.Size(56, 17);
            this.rbSpilletidBegge.TabIndex = 7;
            this.rbSpilletidBegge.Text = "Begge";
            this.rbSpilletidBegge.UseVisualStyleBackColor = true;
            this.rbSpilletidBegge.CheckedChanged += new System.EventHandler(this.rbSpilletidBegge_CheckedChanged);
            // 
            // rbSpilletidHalv
            // 
            this.rbSpilletidHalv.AutoSize = true;
            this.rbSpilletidHalv.Location = new System.Drawing.Point(80, 19);
            this.rbSpilletidHalv.Name = "rbSpilletidHalv";
            this.rbSpilletidHalv.Size = new System.Drawing.Size(58, 17);
            this.rbSpilletidHalv.TabIndex = 6;
            this.rbSpilletidHalv.Text = "Halvtid";
            this.rbSpilletidHalv.UseVisualStyleBackColor = true;
            this.rbSpilletidHalv.CheckedChanged += new System.EventHandler(this.rbSpilletidHalv_CheckedChanged);
            // 
            // rbSpilletidFull
            // 
            this.rbSpilletidFull.AutoSize = true;
            this.rbSpilletidFull.Checked = true;
            this.rbSpilletidFull.Location = new System.Drawing.Point(12, 19);
            this.rbSpilletidFull.Name = "rbSpilletidFull";
            this.rbSpilletidFull.Size = new System.Drawing.Size(52, 17);
            this.rbSpilletidFull.TabIndex = 5;
            this.rbSpilletidFull.TabStop = true;
            this.rbSpilletidFull.Text = "Fulltid";
            this.rbSpilletidFull.UseVisualStyleBackColor = true;
            this.rbSpilletidFull.CheckedChanged += new System.EventHandler(this.rbSpilletidFull_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbTrekningTypeMidtuke);
            this.groupBox2.Controls.Add(this.rbTrekningTypeSøndag);
            this.groupBox2.Controls.Add(this.rbTrekningTypeLørdag);
            this.groupBox2.Location = new System.Drawing.Point(12, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(245, 51);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Spilledag";
            // 
            // rbTrekningTypeMidtuke
            // 
            this.rbTrekningTypeMidtuke.AutoSize = true;
            this.rbTrekningTypeMidtuke.Location = new System.Drawing.Point(163, 19);
            this.rbTrekningTypeMidtuke.Name = "rbTrekningTypeMidtuke";
            this.rbTrekningTypeMidtuke.Size = new System.Drawing.Size(63, 17);
            this.rbTrekningTypeMidtuke.TabIndex = 4;
            this.rbTrekningTypeMidtuke.Text = "Midtuke";
            this.rbTrekningTypeMidtuke.UseVisualStyleBackColor = true;
            this.rbTrekningTypeMidtuke.CheckedChanged += new System.EventHandler(this.rbTrekningTypeMidtuke_CheckedChanged);
            // 
            // rbTrekningTypeSøndag
            // 
            this.rbTrekningTypeSøndag.AutoSize = true;
            this.rbTrekningTypeSøndag.Location = new System.Drawing.Point(80, 19);
            this.rbTrekningTypeSøndag.Name = "rbTrekningTypeSøndag";
            this.rbTrekningTypeSøndag.Size = new System.Drawing.Size(62, 17);
            this.rbTrekningTypeSøndag.TabIndex = 3;
            this.rbTrekningTypeSøndag.Text = "Søndag";
            this.rbTrekningTypeSøndag.UseVisualStyleBackColor = true;
            this.rbTrekningTypeSøndag.CheckedChanged += new System.EventHandler(this.rbTrekningTypeSøndag_CheckedChanged);
            // 
            // rbTrekningTypeLørdag
            // 
            this.rbTrekningTypeLørdag.AutoSize = true;
            this.rbTrekningTypeLørdag.Checked = true;
            this.rbTrekningTypeLørdag.Location = new System.Drawing.Point(12, 19);
            this.rbTrekningTypeLørdag.Name = "rbTrekningTypeLørdag";
            this.rbTrekningTypeLørdag.Size = new System.Drawing.Size(58, 17);
            this.rbTrekningTypeLørdag.TabIndex = 2;
            this.rbTrekningTypeLørdag.TabStop = true;
            this.rbTrekningTypeLørdag.Text = "Lørdag";
            this.rbTrekningTypeLørdag.UseVisualStyleBackColor = true;
            this.rbTrekningTypeLørdag.CheckedChanged += new System.EventHandler(this.rbTrekningTypeLørdag_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtSpillerNummer);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.txtSpillerId);
            this.groupBox4.Location = new System.Drawing.Point(12, 216);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(245, 94);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Spiller identifikasjon";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Identifikasjon:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Spillernummer:";
            // 
            // txtSpillerId
            // 
            this.txtSpillerId.Location = new System.Drawing.Point(84, 55);
            this.txtSpillerId.MaxLength = 20;
            this.txtSpillerId.Name = "txtSpillerId";
            this.txtSpillerId.Size = new System.Drawing.Size(150, 20);
            this.txtSpillerId.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnVelgKatalog);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtFilnavn);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lblKatalog);
            this.groupBox3.Location = new System.Drawing.Point(12, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(245, 83);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Fil informasjon";
            // 
            // btnVelgKatalog
            // 
            this.btnVelgKatalog.AutoSize = true;
            this.btnVelgKatalog.FlatAppearance.BorderSize = 0;
            this.btnVelgKatalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVelgKatalog.Image = ((System.Drawing.Image)(resources.GetObject("btnVelgKatalog.Image")));
            this.btnVelgKatalog.Location = new System.Drawing.Point(211, 24);
            this.btnVelgKatalog.Margin = new System.Windows.Forms.Padding(0);
            this.btnVelgKatalog.Name = "btnVelgKatalog";
            this.btnVelgKatalog.Size = new System.Drawing.Size(28, 22);
            this.btnVelgKatalog.TabIndex = 6;
            this.btnVelgKatalog.UseVisualStyleBackColor = true;
            this.btnVelgKatalog.Click += new System.EventHandler(this.btnVelgKatalog_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = ".NTF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Filnavn:";
            // 
            // txtFilnavn
            // 
            this.txtFilnavn.Location = new System.Drawing.Point(56, 53);
            this.txtFilnavn.MaxLength = 8;
            this.txtFilnavn.Name = "txtFilnavn";
            this.txtFilnavn.Size = new System.Drawing.Size(152, 20);
            this.txtFilnavn.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Katalog:";
            // 
            // lblKatalog
            // 
            this.lblKatalog.AutoEllipsis = true;
            this.lblKatalog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKatalog.Location = new System.Drawing.Point(58, 24);
            this.lblKatalog.Name = "lblKatalog";
            this.lblKatalog.Size = new System.Drawing.Size(150, 20);
            this.lblKatalog.TabIndex = 0;
            this.lblKatalog.Text = "A:\\NTFILE";
            this.lblKatalog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DlgTipperuLagreOnline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(266, 348);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnLagre);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgTipperuLagreOnline";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lagre Online";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtSpillerNummer;
        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSpilletidBegge;
        private System.Windows.Forms.RadioButton rbSpilletidHalv;
        private System.Windows.Forms.RadioButton rbSpilletidFull;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbTrekningTypeMidtuke;
        private System.Windows.Forms.RadioButton rbTrekningTypeSøndag;
        private System.Windows.Forms.RadioButton rbTrekningTypeLørdag;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSpillerId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblKatalog;
        private System.Windows.Forms.TextBox txtFilnavn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnVelgKatalog;
    }
}