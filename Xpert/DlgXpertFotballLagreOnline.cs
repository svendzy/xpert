﻿using System;
using System.Windows.Forms;
using System.IO;

using Xpert.Modell.Objekter;
using Xpert.Modell.Objekter.Tipping;

namespace Xpert
{
    public partial class DlgXpertFotballLagreOnline : Form
    {
        private XpertTippingOnlineFil.TippingTrekningstype trekningType;
        private XpertTippingOnlineFil.TippingSpilletid spilletid;

        private string[] kataloger = new string[]
        {
            XpertKonfigurasjon.KatalogNTF,
            XpertKonfigurasjon.KatalogXpertOnline,
            XpertKonfigurasjon.KatalogDesktop
        };

        public DlgXpertFotballLagreOnline()
        {
            InitializeComponent();

            this.trekningType = XpertTippingOnlineFil.TippingTrekningstype.Lørdag;
            this.spilletid = XpertTippingOnlineFil.TippingSpilletid.Full;
        }

        public string Filnavn
        {
            get
            {
                return Path.Combine(lblKatalog.Text, Path.ChangeExtension(txtFilnavn.Text, ".NTF"));
            }
        }

        public XpertTippingOnlineFil.TippingTrekningstype TrekningType
        {
            get
            {
                return this.trekningType;
            }
        }

        public XpertTippingOnlineFil.TippingSpilletid Spilletid
        {
            get
            {
                return this.spilletid;
            }
        }

        public string SpillerNummer
        {
            get
            {
                return txtSpillerNummer.Text;
            }
        }

        public string SpillerId
        {
            get
            {
                return txtSpillerId.Text;
            }
        }

        private void btnVelgKatalog_Click(object sender, EventArgs e)
        {
            if (lblKatalog.Text.Equals(kataloger[0]))
            {
                lblKatalog.Text = kataloger[1];
            }
            else if (lblKatalog.Text.Equals(kataloger[1]))
            {
                lblKatalog.Text = kataloger[2];
            }
            else if (lblKatalog.Text.Equals(kataloger[2]))
            {
                lblKatalog.Text = kataloger[0];
            }
        }

        private void rbTrekningTypeLørdag_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTrekningTypeLørdag.Checked)
            {
                this.trekningType = XpertTippingOnlineFil.TippingTrekningstype.Lørdag;
            }
        }

        private void rbTrekningTypeSøndag_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTrekningTypeSøndag.Checked)
            {
                this.trekningType = XpertTippingOnlineFil.TippingTrekningstype.Søndag;
            }
        }

        private void rbTrekningTypeMidtuke_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTrekningTypeMidtuke.Checked)
            {
                this.trekningType = XpertTippingOnlineFil.TippingTrekningstype.Midtuke;
            }
        }

        private void rbSpilletidFull_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSpilletidFull.Checked)
            {
                this.spilletid = XpertTippingOnlineFil.TippingSpilletid.Full;
            }
        }

        private void rbSpilletidHalv_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSpilletidHalv.Checked)
            {
                this.spilletid = XpertTippingOnlineFil.TippingSpilletid.Halv;
            }
        }

        private void rbSpilletidBegge_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSpilletidBegge.Checked)
            {
                this.spilletid = XpertTippingOnlineFil.TippingSpilletid.Begge;
            }
        }
    }
}
