﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Xpert
{
    public partial class FrmXpertHoved : Form
    {
        MdiClient mdiClient = null;

        public FrmXpertHoved()
        {
            InitializeComponent();
            mdiClient = FindMdiClient();
            mdiClient.BackColor = Color.White;
        }

        private MdiClient FindMdiClient()
        {
            for (int n = 0; n < Controls.Count; n++)
            {
                if (Controls[n] is MdiClient)
                    return Controls[n] as MdiClient;
            }
            return null;
        }

        private void tsNyttSystem_Click(object sender, EventArgs e)
        {
            FrmXpertFotballSystem frmXpertFotballSystem = new FrmXpertFotballSystem();
            frmXpertFotballSystem.SystemFil = String.Empty;
            frmXpertFotballSystem.Text = "System";
          //  frmXpertFotballSystem.WindowState = FormWindowState.Maximized;
            frmXpertFotballSystem.MdiParent = this;
            frmXpertFotballSystem.Show();
        }

        private void tsHentSystem_Click(object sender, EventArgs e)
        {
            DlgXpertHentSystem dlgHentSystem = new DlgXpertHentSystem();
            DialogResult result = dlgHentSystem.ShowDialog();
            if (result == DialogResult.OK)
            {
                switch (dlgHentSystem.SystemType)
                {
                    case Xpert.Modell.Objekter.XpertSystemType.Fotball:
                        FrmXpertFotballSystem frmXpertFotballSystem = new FrmXpertFotballSystem();
                        frmXpertFotballSystem.SystemFil = dlgHentSystem.Filnavn;
                        frmXpertFotballSystem.Text = "System";
                      //  frmXpertFotballSystem.WindowState = FormWindowState.Maximized;
                        frmXpertFotballSystem.MdiParent = this;
                        frmXpertFotballSystem.Show();
                        break;
                }
            }
        }

        protected override void OnMdiChildActivate(EventArgs e)
        {
            base.OnMdiChildActivate(e);
            ToolStripManager.RevertMerge(tsXpertHoved);

            if (ActiveMdiChild is FrmXpertFotballSystem)
            {
                ActiveMdiChild.WindowState = FormWindowState.Maximized;
                ToolStripManager.Merge(((FrmXpertFotballSystem)ActiveMdiChild).ToolStrip, tsXpertHoved);
            }
        }

        private void tsSystemer_DropDownOpening(object sender, EventArgs e)
        {
            tsSystemer.DropDownItems.Clear();
            for (int nMdiChild = 0; nMdiChild < this.MdiChildren.Length; nMdiChild++)
            {
                if (this.ActiveMdiChild.Equals(MdiChildren[nMdiChild]))
                {
                    ToolStripItem mnuItem = new ToolStripMenuItem();
                    mnuItem.Text = this.MdiChildren[nMdiChild].Text;
                    mnuItem.ForeColor = Color.Blue;
                    tsSystemer.DropDownItems.Add(mnuItem);
                }
                else
                {
                    ToolStripItem mnuItem = new ToolStripMenuItem();
                    mnuItem.Text = this.MdiChildren[nMdiChild].Text;
                    mnuItem.ForeColor = Color.Black;
                    tsSystemer.DropDownItems.Add(mnuItem);
                }
            }
        }

        private void tsSystemer_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int index = tsSystemer.DropDownItems.IndexOf(e.ClickedItem);
            if (index != -1)
            {
                this.MdiChildren[index].WindowState = FormWindowState.Maximized;
                this.MdiChildren[index].Focus();
            }
        }

        private void mnuFilHentSystem_Click(object sender, EventArgs e)
        {
            tsHentSystem_Click(null, null);
        }

        private void mnuFilNyttSystem_Click(object sender, EventArgs e)
        {
            tsNyttSystem_Click(null, null);
        }

        private void mnuFilAvslutt_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
