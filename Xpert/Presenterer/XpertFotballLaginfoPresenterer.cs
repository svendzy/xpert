﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Xpert.Visninger;
using Xpert.Modell.Objekter;
using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Presenterer
{
    public class XpertFotballLaginfoPresenterer : Presenterer<IXpertFotballLaginfoVisning>
    {
        public XpertFotballLaginfoPresenterer(IXpertFotballLaginfoVisning visning) : base(visning) { }

        public void Vis()
        {
            string lagInfoFil = Path.Combine(XpertKonfigurasjon.KatalogXpert, "laginfo.txt");
            Dictionary<string, ArrayList> lagInfo = new Dictionary<string, ArrayList>();
            using (StreamReader reader = File.OpenText(lagInfoFil))
            {
                string linje = String.Empty;
                ArrayList lag = null;
                while ((linje = reader.ReadLine()) != null)
                {
                    if (linje.StartsWith("#"))
                    {
                        lag = new ArrayList();
                        string land = linje.TrimStart(new char[] { '#' });
                        lagInfo[land] = lag;
                    }
                    else
                    {
                        if ((lag != null) && (linje.Trim() != String.Empty))
                            lag.Add(linje);
                    }
                }
            }
            Visning.LagInfo = lagInfo;
        }
    }
}
