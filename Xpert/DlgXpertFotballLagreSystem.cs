﻿using System;
using System.IO;
using System.Windows.Forms;

using Xpert.Modell.Objekter;

namespace Xpert
{
    public partial class DlgXpertFotballLagreSystem : Form
    {
        string dataKatalog;

        public DlgXpertFotballLagreSystem()
        {
            InitializeComponent();

            dataKatalog = XpertKonfigurasjon.KatalogXpertData;
            lblKatalog.Text = dataKatalog;
            if (!Directory.Exists(dataKatalog))
                Directory.CreateDirectory(dataKatalog);

            string[] filer = Directory.GetFiles(dataKatalog, "*.xpf", SearchOption.TopDirectoryOnly);
            for (int nFil = 0; nFil < filer.Length; nFil++)
            {
                lbxSystemer.Items.Add(Path.GetFileName(filer[nFil]));
            }
        }

        public string Filnavn
        {
            get
            {
                return Path.Combine(dataKatalog, Path.ChangeExtension(txtFilnavn.Text, "xpf"));
            }
        }

        private void lbxSystemer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxSystemer.SelectedIndex != -1)
            {
                txtFilnavn.Text = Convert.ToString(lbxSystemer.SelectedItem);
            }
        }
    }
}
