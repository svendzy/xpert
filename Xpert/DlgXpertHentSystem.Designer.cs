﻿namespace Xpert
{
    partial class DlgXpertHentSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHent = new System.Windows.Forms.Button();
            this.lbxSystemer = new System.Windows.Forms.ListBox();
            this.txtFilnavn = new System.Windows.Forms.TextBox();
            this.lblFilnavn = new System.Windows.Forms.Label();
            this.lblFiltype = new System.Windows.Forms.Label();
            this.lblKatalog = new System.Windows.Forms.Label();
            this.cbxFiltype = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnHent
            // 
            this.btnHent.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnHent.Location = new System.Drawing.Point(205, 279);
            this.btnHent.Name = "btnHent";
            this.btnHent.Size = new System.Drawing.Size(75, 23);
            this.btnHent.TabIndex = 0;
            this.btnHent.Text = "Hent";
            this.btnHent.UseVisualStyleBackColor = true;
            // 
            // lbxSystemer
            // 
            this.lbxSystemer.FormattingEnabled = true;
            this.lbxSystemer.Location = new System.Drawing.Point(6, 57);
            this.lbxSystemer.Name = "lbxSystemer";
            this.lbxSystemer.Size = new System.Drawing.Size(274, 186);
            this.lbxSystemer.TabIndex = 1;
            this.lbxSystemer.SelectedIndexChanged += new System.EventHandler(this.lbxSystemer_SelectedIndexChanged);
            // 
            // txtFilnavn
            // 
            this.txtFilnavn.Location = new System.Drawing.Point(53, 249);
            this.txtFilnavn.Name = "txtFilnavn";
            this.txtFilnavn.Size = new System.Drawing.Size(227, 20);
            this.txtFilnavn.TabIndex = 3;
            // 
            // lblFilnavn
            // 
            this.lblFilnavn.AutoSize = true;
            this.lblFilnavn.Location = new System.Drawing.Point(3, 252);
            this.lblFilnavn.Name = "lblFilnavn";
            this.lblFilnavn.Size = new System.Drawing.Size(41, 13);
            this.lblFilnavn.TabIndex = 4;
            this.lblFilnavn.Text = "Filnavn";
            // 
            // lblFiltype
            // 
            this.lblFiltype.AutoSize = true;
            this.lblFiltype.Location = new System.Drawing.Point(3, 36);
            this.lblFiltype.Name = "lblFiltype";
            this.lblFiltype.Size = new System.Drawing.Size(37, 13);
            this.lblFiltype.TabIndex = 9;
            this.lblFiltype.Text = "Filtype";
            // 
            // lblKatalog
            // 
            this.lblKatalog.AutoEllipsis = true;
            this.lblKatalog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKatalog.Location = new System.Drawing.Point(6, 9);
            this.lblKatalog.Name = "lblKatalog";
            this.lblKatalog.Size = new System.Drawing.Size(274, 13);
            this.lblKatalog.TabIndex = 11;
            // 
            // cbxFiltype
            // 
            this.cbxFiltype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFiltype.Enabled = false;
            this.cbxFiltype.FormattingEnabled = true;
            this.cbxFiltype.Items.AddRange(new object[] {
            "Xpert fotball systemer *.xpf | (*.xpf)"});
            this.cbxFiltype.Location = new System.Drawing.Point(53, 33);
            this.cbxFiltype.Name = "cbxFiltype";
            this.cbxFiltype.Size = new System.Drawing.Size(227, 21);
            this.cbxFiltype.TabIndex = 12;
            this.cbxFiltype.SelectedIndexChanged += new System.EventHandler(this.cbxFiltype_SelectedIndexChanged);
            // 
            // DlgXpertHentSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 308);
            this.Controls.Add(this.cbxFiltype);
            this.Controls.Add(this.lblKatalog);
            this.Controls.Add(this.lblFiltype);
            this.Controls.Add(this.lblFilnavn);
            this.Controls.Add(this.txtFilnavn);
            this.Controls.Add(this.lbxSystemer);
            this.Controls.Add(this.btnHent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgXpertHentSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Hent system";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHent;
        private System.Windows.Forms.ListBox lbxSystemer;
        private System.Windows.Forms.TextBox txtFilnavn;
        private System.Windows.Forms.Label lblFilnavn;
        private System.Windows.Forms.Label lblFiltype;
        private System.Windows.Forms.Label lblKatalog;
        private System.Windows.Forms.ComboBox cbxFiltype;
    }
}