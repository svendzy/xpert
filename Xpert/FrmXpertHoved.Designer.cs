﻿namespace Xpert
{
    partial class FrmXpertHoved
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmXpertHoved));
            this.msXpertHoved = new System.Windows.Forms.MenuStrip();
            this.mnuFil = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilNyttSystem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilHentSystem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilAvslutt = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ssXpertHoved = new System.Windows.Forms.StatusStrip();
            this.tsXpertHoved = new System.Windows.Forms.ToolStrip();
            this.tsNyttSystem = new System.Windows.Forms.ToolStripButton();
            this.tsHentSystem = new System.Windows.Forms.ToolStripButton();
            this.tsSystemer = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.msXpertHoved.SuspendLayout();
            this.tsXpertHoved.SuspendLayout();
            this.SuspendLayout();
            // 
            // msXpertHoved
            // 
            this.msXpertHoved.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFil,
            this.toolStripMenuItem1});
            this.msXpertHoved.Location = new System.Drawing.Point(0, 0);
            this.msXpertHoved.Name = "msXpertHoved";
            this.msXpertHoved.Size = new System.Drawing.Size(689, 24);
            this.msXpertHoved.TabIndex = 1;
            // 
            // mnuFil
            // 
            this.mnuFil.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFilNyttSystem,
            this.mnuFilHentSystem,
            this.mnuFilAvslutt});
            this.mnuFil.Name = "mnuFil";
            this.mnuFil.Size = new System.Drawing.Size(31, 20);
            this.mnuFil.Text = "&Fil";
            // 
            // mnuFilNyttSystem
            // 
            this.mnuFilNyttSystem.Name = "mnuFilNyttSystem";
            this.mnuFilNyttSystem.Size = new System.Drawing.Size(152, 22);
            this.mnuFilNyttSystem.Text = "&Nytt system";
            this.mnuFilNyttSystem.Click += new System.EventHandler(this.mnuFilNyttSystem_Click);
            // 
            // mnuFilHentSystem
            // 
            this.mnuFilHentSystem.Name = "mnuFilHentSystem";
            this.mnuFilHentSystem.Size = new System.Drawing.Size(152, 22);
            this.mnuFilHentSystem.Text = "&Hent system...";
            this.mnuFilHentSystem.Click += new System.EventHandler(this.mnuFilHentSystem_Click);
            // 
            // mnuFilAvslutt
            // 
            this.mnuFilAvslutt.Name = "mnuFilAvslutt";
            this.mnuFilAvslutt.Size = new System.Drawing.Size(152, 22);
            this.mnuFilAvslutt.Text = "&Avslutt";
            this.mnuFilAvslutt.Click += new System.EventHandler(this.mnuFilAvslutt_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // ssXpertHoved
            // 
            this.ssXpertHoved.Location = new System.Drawing.Point(0, 473);
            this.ssXpertHoved.Name = "ssXpertHoved";
            this.ssXpertHoved.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.ssXpertHoved.Size = new System.Drawing.Size(689, 22);
            this.ssXpertHoved.TabIndex = 2;
            // 
            // tsXpertHoved
            // 
            this.tsXpertHoved.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsNyttSystem,
            this.tsHentSystem,
            this.tsSystemer,
            this.toolStripSeparator1});
            this.tsXpertHoved.Location = new System.Drawing.Point(0, 24);
            this.tsXpertHoved.Name = "tsXpertHoved";
            this.tsXpertHoved.Size = new System.Drawing.Size(689, 25);
            this.tsXpertHoved.TabIndex = 3;
            this.tsXpertHoved.Text = "toolStrip1";
            // 
            // tsNyttSystem
            // 
            this.tsNyttSystem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsNyttSystem.Image = ((System.Drawing.Image)(resources.GetObject("tsNyttSystem.Image")));
            this.tsNyttSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsNyttSystem.Name = "tsNyttSystem";
            this.tsNyttSystem.Size = new System.Drawing.Size(23, 22);
            this.tsNyttSystem.Text = "&Nytt system";
            this.tsNyttSystem.ToolTipText = "Nytt system";
            this.tsNyttSystem.Click += new System.EventHandler(this.tsNyttSystem_Click);
            // 
            // tsHentSystem
            // 
            this.tsHentSystem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsHentSystem.Image = ((System.Drawing.Image)(resources.GetObject("tsHentSystem.Image")));
            this.tsHentSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsHentSystem.Name = "tsHentSystem";
            this.tsHentSystem.Size = new System.Drawing.Size(23, 22);
            this.tsHentSystem.Text = "Hent system";
            this.tsHentSystem.Click += new System.EventHandler(this.tsHentSystem_Click);
            // 
            // tsSystemer
            // 
            this.tsSystemer.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsSystemer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSystemer.Image = ((System.Drawing.Image)(resources.GetObject("tsSystemer.Image")));
            this.tsSystemer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSystemer.Name = "tsSystemer";
            this.tsSystemer.Size = new System.Drawing.Size(29, 22);
            this.tsSystemer.Text = "Systemer";
            this.tsSystemer.DropDownOpening += new System.EventHandler(this.tsSystemer_DropDownOpening);
            this.tsSystemer.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsSystemer_DropDownItemClicked);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // FrmXpertHoved
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(689, 495);
            this.Controls.Add(this.tsXpertHoved);
            this.Controls.Add(this.ssXpertHoved);
            this.Controls.Add(this.msXpertHoved);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.msXpertHoved;
            this.Name = "FrmXpertHoved";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xpert Systemtipping v1.0";
            this.msXpertHoved.ResumeLayout(false);
            this.msXpertHoved.PerformLayout();
            this.tsXpertHoved.ResumeLayout(false);
            this.tsXpertHoved.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msXpertHoved;
        private System.Windows.Forms.StatusStrip ssXpertHoved;
        private System.Windows.Forms.ToolStripMenuItem mnuFil;
        private System.Windows.Forms.ToolStrip tsXpertHoved;
        private System.Windows.Forms.ToolStripMenuItem mnuFilNyttSystem;
        private System.Windows.Forms.ToolStripMenuItem mnuFilHentSystem;
        private System.Windows.Forms.ToolStripMenuItem mnuFilAvslutt;
        private System.Windows.Forms.ToolStripButton tsHentSystem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton tsNyttSystem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton tsSystemer;
    }
}