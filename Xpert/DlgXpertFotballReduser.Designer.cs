﻿namespace Xpert
{
    partial class DlgTipperuReduser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbGaranti = new System.Windows.Forms.GroupBox();
            this.rbGarantiAlltid12rette = new System.Windows.Forms.RadioButton();
            this.rbGaranti10Rette = new System.Windows.Forms.RadioButton();
            this.rbGaranti11Rette = new System.Windows.Forms.RadioButton();
            this.rbGaranti9Rette = new System.Windows.Forms.RadioButton();
            this.btnOK = new System.Windows.Forms.Button();
            this.gbGaranti.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbGaranti
            // 
            this.gbGaranti.Controls.Add(this.rbGarantiAlltid12rette);
            this.gbGaranti.Controls.Add(this.rbGaranti10Rette);
            this.gbGaranti.Controls.Add(this.rbGaranti11Rette);
            this.gbGaranti.Controls.Add(this.rbGaranti9Rette);
            this.gbGaranti.Location = new System.Drawing.Point(1, 3);
            this.gbGaranti.Name = "gbGaranti";
            this.gbGaranti.Size = new System.Drawing.Size(318, 57);
            this.gbGaranti.TabIndex = 0;
            this.gbGaranti.TabStop = false;
            this.gbGaranti.Text = "Garanti";
            // 
            // rbGarantiAlltid12rette
            // 
            this.rbGarantiAlltid12rette.AutoSize = true;
            this.rbGarantiAlltid12rette.Checked = true;
            this.rbGarantiAlltid12rette.Location = new System.Drawing.Point(239, 24);
            this.rbGarantiAlltid12rette.Name = "rbGarantiAlltid12rette";
            this.rbGarantiAlltid12rette.Size = new System.Drawing.Size(62, 17);
            this.rbGarantiAlltid12rette.TabIndex = 3;
            this.rbGarantiAlltid12rette.TabStop = true;
            this.rbGarantiAlltid12rette.Text = "Alltid 12";
            this.rbGarantiAlltid12rette.UseVisualStyleBackColor = true;
            this.rbGarantiAlltid12rette.CheckedChanged += new System.EventHandler(this.rbGarantiAlltid12rette_CheckedChanged);
            // 
            // rbGaranti10Rette
            // 
            this.rbGaranti10Rette.AutoSize = true;
            this.rbGaranti10Rette.Location = new System.Drawing.Point(78, 24);
            this.rbGaranti10Rette.Name = "rbGaranti10Rette";
            this.rbGaranti10Rette.Size = new System.Drawing.Size(61, 17);
            this.rbGaranti10Rette.TabIndex = 2;
            this.rbGaranti10Rette.Text = "10 rette";
            this.rbGaranti10Rette.UseVisualStyleBackColor = true;
            this.rbGaranti10Rette.CheckedChanged += new System.EventHandler(this.rbGaranti10Rette_CheckedChanged);
            // 
            // rbGaranti11Rette
            // 
            this.rbGaranti11Rette.AutoSize = true;
            this.rbGaranti11Rette.Location = new System.Drawing.Point(161, 24);
            this.rbGaranti11Rette.Name = "rbGaranti11Rette";
            this.rbGaranti11Rette.Size = new System.Drawing.Size(61, 17);
            this.rbGaranti11Rette.TabIndex = 1;
            this.rbGaranti11Rette.Text = "11 rette";
            this.rbGaranti11Rette.UseVisualStyleBackColor = true;
            this.rbGaranti11Rette.CheckedChanged += new System.EventHandler(this.rbGaranti11Rette_CheckedChanged);
            // 
            // rbGaranti9Rette
            // 
            this.rbGaranti9Rette.AutoSize = true;
            this.rbGaranti9Rette.Location = new System.Drawing.Point(7, 24);
            this.rbGaranti9Rette.Name = "rbGaranti9Rette";
            this.rbGaranti9Rette.Size = new System.Drawing.Size(55, 17);
            this.rbGaranti9Rette.TabIndex = 0;
            this.rbGaranti9Rette.Text = "9 rette";
            this.rbGaranti9Rette.UseVisualStyleBackColor = true;
            this.rbGaranti9Rette.CheckedChanged += new System.EventHandler(this.rbGaranti9Rette_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(244, 187);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // DlgTipperuReduser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(322, 218);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbGaranti);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgTipperuReduser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reduser rekker";
            this.gbGaranti.ResumeLayout(false);
            this.gbGaranti.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbGaranti;
        private System.Windows.Forms.RadioButton rbGaranti10Rette;
        private System.Windows.Forms.RadioButton rbGaranti11Rette;
        private System.Windows.Forms.RadioButton rbGaranti9Rette;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.RadioButton rbGarantiAlltid12rette;
    }
}