﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingKupong : UserControl
    {
        private int kupongNr = 1;
        private XpertTippingRekker rekker;

        Rectangle[,] pos = new Rectangle[12, 3] {
            {new Rectangle(4,15,13,15), new Rectangle(23,15,13,15), new Rectangle(42,15,13,15)},
            {new Rectangle(4,36,13,15), new Rectangle(23,36,13,15), new Rectangle(42,36,13,15)},
            {new Rectangle(4,57,13,15), new Rectangle(23,57,13,15), new Rectangle(42,57,13,15)},
            {new Rectangle(4,78,13,15), new Rectangle(23,78,13,15), new Rectangle(42,78,13,15)},
            {new Rectangle(4,99,13,15), new Rectangle(23,99,13,15), new Rectangle(42,99,13,15)},
            {new Rectangle(4,120,13,15), new Rectangle(23,120,13,15), new Rectangle(42,120,13,15)},
            {new Rectangle(4,141,13,15), new Rectangle(23,141,13,15), new Rectangle(42,141,13,15)},
            {new Rectangle(4,162,13,15), new Rectangle(23,162,13,15), new Rectangle(42,162,13,15)},
            {new Rectangle(4,183,13,15), new Rectangle(23,183,13,15), new Rectangle(42,183,13,15)},
            {new Rectangle(4,204,13,15), new Rectangle(23,204,13,15), new Rectangle(42,204,13,15)},
            {new Rectangle(4,225,13,15), new Rectangle(23,225,13,15), new Rectangle(42,225,13,15)},
            {new Rectangle(4,246,13,15), new Rectangle(23,246,13,15), new Rectangle(42,246,13,15)}
        };

        public UscXpertTippingKupong()
        {
            InitializeComponent();

            rekker = new XpertTippingRekker();
        }

        public XpertTippingRekker Rekker
        {
            get
            {
                return rekker;
            }
            set
            {
                rekker = value;
            }
        }

        private int SisteKupongNr
        {
            get
            {
                int rekkerSisteKupong = rekker.Count % 10;
                return (rekkerSisteKupong == 0) ? (rekker.Count / 10) : ((rekker.Count / 10) + 1);
            }
        }

        public void BlaFørsteKupong()
        {
            kupongNr = 1;
            Invalidate();
        }

        public void BlaSisteKupong()
        {
            kupongNr = SisteKupongNr;
            Invalidate();
        }

        public void BlaNesteKupong()
        {
            kupongNr += (kupongNr < SisteKupongNr) ? 1 : 0;
            Invalidate();
        }

        public void BlaForrigeKupong()
        {
            kupongNr -= (kupongNr > 1) ? 1 : 0;
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (rekker != null)
            {
                int offsetX = 0;
                int rekkeIndex = (kupongNr - 1) * 10;
                int sisteRekkeIndex = ((rekkeIndex + 10) > rekker.Count) ? rekker.Count : rekkeIndex + 10;
                for (int nRekke = rekkeIndex; nRekke < sisteRekkeIndex; nRekke++)
                {
                    for (int nMarkering = 0; nMarkering < 12; nMarkering++)
                    {
                        Rectangle rect_0 = pos[nMarkering, 0];
                        rect_0.Offset(offsetX, 0);
                        Rectangle rect_1 = pos[nMarkering, 1];
                        rect_1.Offset(offsetX, 0);
                        Rectangle rect_2 = pos[nMarkering, 2];
                        rect_2.Offset(offsetX, 0);

                        switch (rekker[nRekke].Markering[nMarkering])
                        {
                            case 1:
                                if (rect_2.IntersectsWith(e.ClipRectangle))
                                    e.Graphics.DrawImage(Properties.Resources.kryss, rect_2);
                                break;
                            case 2:
                                if (rect_1.IntersectsWith(e.ClipRectangle))
                                    e.Graphics.DrawImage(Properties.Resources.kryss, rect_1);
                                break;
                            case 4:
                                if (rect_0.IntersectsWith(e.ClipRectangle))
                                    e.Graphics.DrawImage(Properties.Resources.kryss, rect_0);
                                break;
                        }
                    }
                    offsetX += 59;
                }
            }
        }
    }
}
