﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingTegnFordeling : UserControl
    {
        private XpertTippingFordeling data = new XpertTippingFordeling();

        Rectangle[,] pos = new Rectangle[13, 4] {
{new Rectangle(4, 24, 13, 15), new Rectangle(23, 24, 13, 15), new Rectangle(42, 24, 13, 15), new Rectangle(61,24,13,15)},
{new Rectangle(4, 45, 13, 15), new Rectangle(23, 45, 13, 15), new Rectangle(42, 45, 13, 15), new Rectangle(61,45,13,15)},
{new Rectangle(4, 66, 13, 15), new Rectangle(23, 66, 13, 15), new Rectangle(42, 66, 13, 15), new Rectangle(61,66,13,15)},
{new Rectangle(4, 87, 13, 15), new Rectangle(23, 87, 13, 15), new Rectangle(42, 87, 13, 15), new Rectangle(61,87,13,15)},
{new Rectangle(4, 108, 13, 15), new Rectangle(23, 108, 13, 15), new Rectangle(42, 108, 13, 15), new Rectangle(61,108,13,15)},
{new Rectangle(4, 129, 13, 15), new Rectangle(23, 129, 13, 15), new Rectangle(42, 129, 13, 15), new Rectangle(61,129,13,15)},
{new Rectangle(4, 150, 13, 15), new Rectangle(23, 150, 13, 15), new Rectangle(42, 150, 13, 15), new Rectangle(61,150,13,15)},
{new Rectangle(4, 171, 13, 15), new Rectangle(23, 171, 13, 15), new Rectangle(42, 171, 13, 15), new Rectangle(61,171,13,15)},
{new Rectangle(4, 192, 13, 15), new Rectangle(23, 192, 13, 15), new Rectangle(42, 192, 13, 15), new Rectangle(61,192,13,15)},
{new Rectangle(4, 213, 13, 15), new Rectangle(23, 213, 13, 15), new Rectangle(42, 213, 13, 15), new Rectangle(61,213,13,15)},
{new Rectangle(4, 234, 13, 15), new Rectangle(23, 234, 13, 15), new Rectangle(42, 234, 13, 15), new Rectangle(61,234,13,15)},
{new Rectangle(4, 255, 13, 15), new Rectangle(23, 255, 13, 15), new Rectangle(42, 255, 13, 15), new Rectangle(61,255,13,15)},
{new Rectangle(4, 276, 13, 15), new Rectangle(23, 276, 13, 15), new Rectangle(42, 276, 13, 15), new Rectangle(61,276,13,15)}
        };

        public UscXpertTippingTegnFordeling()
        {
            InitializeComponent();
        }

        public XpertTippingFordeling Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Beskrivelse
        {
            get
            {
                return lblBeskrivelse.Text;
            }
            set
            {
                lblBeskrivelse.Text = value;
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (pos[0, 0].Contains(e.Location))
            {
                data.Invertert = (data.Invertert) ? false : true;
                Invalidate(pos[0, 0]);
                return;
            }

            for (int nMarkert = 0; nMarkert < 12; nMarkert++)
            {
                if (pos[nMarkert + 1, 0].Contains(e.Location))
                {
                    data.Markert[nMarkert] = (data.Markert[nMarkert]) ? false : true;
                    Invalidate();
                    return;
                }
            }

            for (int n = 0; n < 13; n++)
            {
                if ((e.Y >= pos[n, 1].Y) && (e.Y <= pos[n, 1].Bottom))
                {
                    for (int nKolonne = 1; nKolonne <= 3; nKolonne++)
                    {
                        if (pos[n, nKolonne].Contains(e.Location))
                        {
                            switch (nKolonne)
                            {
                                case 1:
                                    data.AntH[n] = (data.AntH[n]) ? false : true;
                                    break;
                                case 2:
                                    data.AntU[n] = (data.AntU[n]) ? false : true;
                                    break;
                                case 3:
                                    data.AntB[n] = (data.AntB[n]) ? false : true;
                                    break;
                            }
                            Invalidate();
                            return;
                        }
                    }
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (data.Invertert)
                e.Graphics.DrawImage(Properties.Resources.kryss_3, pos[0, 0]);
            else
                e.Graphics.DrawImage(Properties.Resources.kryss, pos[0, 0]);

            for (int nMarkert = 0; nMarkert < 12; nMarkert++)
            {
                if (data.Markert[nMarkert])
                    e.Graphics.DrawImage(Properties.Resources.merke, pos[nMarkert + 1, 0]);
            }

            using (Font font = new Font("Fixedsys", 7.5f))
            {
                for (int n = 0; n < 13; n++)
                {
                    SizeF fontSize = e.Graphics.MeasureString(n.ToString(), font);
                    if (data.AntH[n])
                        e.Graphics.DrawString(n.ToString(), font, Brushes.Black, new PointF(pos[n, 1].X + (pos[n, 1].Width / 2) - (fontSize.Width / 2), pos[n, 1].Y + (pos[n, 1].Height / 2) - (fontSize.Height / 2)));
                    if (data.AntU[n])
                        e.Graphics.DrawString(n.ToString(), font, Brushes.Black, new PointF(pos[n, 2].X + (pos[n, 2].Width / 2) - (fontSize.Width / 2), pos[n, 2].Y + (pos[n, 2].Height / 2) - (fontSize.Height / 2)));
                    if (data.AntB[n])
                        e.Graphics.DrawString(n.ToString(), font, Brushes.Black, new PointF(pos[n, 3].X + (pos[n, 3].Width / 2) - (fontSize.Width / 2), pos[n, 3].Y + (pos[n, 3].Height / 2) - (fontSize.Height / 2)));
                }
            }
        }
    }
}
