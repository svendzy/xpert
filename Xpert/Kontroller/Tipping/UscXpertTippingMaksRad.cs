﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingMaksRad : UserControl
    {
        private XpertTippingMaksRad data = new XpertTippingMaksRad();

        Rectangle[,] pos = new Rectangle[12, 3] {
{new Rectangle(4, 24, 13, 15), new Rectangle(23, 24, 13, 15), new Rectangle(42, 24, 13, 15)},
{new Rectangle(4, 45, 13, 15), new Rectangle(23, 45, 13, 15), new Rectangle(42, 45, 13, 15)},
{new Rectangle(4, 66, 13, 15), new Rectangle(23, 66, 13, 15), new Rectangle(42, 66, 13, 15)},
{new Rectangle(4, 87, 13, 15), new Rectangle(23, 87, 13, 15), new Rectangle(42, 87, 13, 15)},
{new Rectangle(4, 108, 13, 15), new Rectangle(23, 108, 13, 15), new Rectangle(42, 108, 13, 15)},
{new Rectangle(4, 129, 13, 15), new Rectangle(23, 129, 13, 15), new Rectangle(42, 129, 13, 15)},
{new Rectangle(4, 150, 13, 15), new Rectangle(23, 150, 13, 15), new Rectangle(42, 150, 13, 15)},
{new Rectangle(4, 171, 13, 15), new Rectangle(23, 171, 13, 15), new Rectangle(42, 171, 13, 15)},
{new Rectangle(4, 192, 13, 15), new Rectangle(23, 192, 13, 15), new Rectangle(42, 192, 13, 15)},
{new Rectangle(4, 213, 13, 15), new Rectangle(23, 213, 13, 15), new Rectangle(42, 213, 13, 15)},
{new Rectangle(4, 234, 13, 15), new Rectangle(23, 234, 13, 15), new Rectangle(42, 234, 13, 15)},
{new Rectangle(4, 255, 13, 15), new Rectangle(23, 255, 13, 15), new Rectangle(42, 255, 13, 15)}
        };

        public UscXpertTippingMaksRad()
        {
            InitializeComponent();
        }

        public XpertTippingMaksRad Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                Invalidate();
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            for (int n = 0; n < 12; n++)
            {
                if ((e.Y >= pos[n, 0].Y) && (e.Y < pos[n, 0].Y + pos[n, 0].Height))
                {
                    for (int nKolonne = 0; nKolonne < 3; nKolonne++)
                    {
                        if (pos[n, nKolonne].Contains(e.Location))
                        {
                            switch (nKolonne)
                            {
                                case 0: data.AntH = n + 1;
                                    break;
                                case 1: data.AntU = n + 1;
                                    break;
                                case 2: data.AntB = n + 1;
                                    break;
                            }
                            Invalidate();
                            return;
                        }
                    }
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            using (Font font = new Font("Fixedsys", 7.5f))
            {
                for (int n = 0; n < data.AntH; n++)
                {
                    SizeF fontSize = e.Graphics.MeasureString((n + 1).ToString(), font);
                    e.Graphics.DrawString((n + 1).ToString(), font, Brushes.Black,
                        new PointF(
                        pos[n, 0].X + (pos[n, 0].Width / 2) - (fontSize.Width / 2),
                        pos[n, 0].Y + (pos[n, 0].Height / 2) - (fontSize.Height / 2)));
                }

                for (int n = 0; n < data.AntU; n++)
                {
                    SizeF fontSize = e.Graphics.MeasureString((n + 1).ToString(), font);
                    e.Graphics.DrawString((n + 1).ToString(), font, Brushes.Black,
                        new PointF(
                        pos[n, 1].X + (pos[n, 1].Width / 2) - (fontSize.Width / 2),
                        pos[n, 1].Y + (pos[n, 1].Height / 2) - (fontSize.Height / 2)));
                }

                for (int n = 0; n < data.AntB; n++)
                {
                    SizeF fontSize = e.Graphics.MeasureString((n + 1).ToString(), font);
                    e.Graphics.DrawString((n + 1).ToString(), font, Brushes.Black,
                        new PointF(
                        pos[n, 2].X + (pos[n, 2].Width / 2) - (fontSize.Width / 2),
                        pos[n, 2].Y + (pos[n, 2].Height / 2) - (fontSize.Height / 2)));
                }
            }
        }

    }
}
