﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingBlokk : UserControl
    {
        private XpertTippingBlokk data;

        Rectangle[,] pos = new Rectangle[12, 3] {
            {new Rectangle(4, 25, 13, 15), new Rectangle(23, 25, 13, 15), new Rectangle(42, 25, 13, 15)},
            {new Rectangle(4, 46, 13, 15), new Rectangle(23, 46, 13, 15), new Rectangle(42, 46, 13, 15)},
            {new Rectangle(4, 67, 13, 15), new Rectangle(23, 67, 13, 15), new Rectangle(42, 67, 13, 15)},
            {new Rectangle(4, 88, 13, 15), new Rectangle(23, 88, 13, 15), new Rectangle(42, 88, 13, 15)},
            {new Rectangle(4, 109, 13, 15), new Rectangle(23, 109, 13, 15), new Rectangle(42, 109, 13, 15)},
            {new Rectangle(4, 130, 13, 15), new Rectangle(23, 130, 13, 15), new Rectangle(42, 130, 13, 15)},
            {new Rectangle(4, 151, 13, 15), new Rectangle(23, 151, 13, 15), new Rectangle(42, 151, 13, 15)},
            {new Rectangle(4, 172, 13, 15), new Rectangle(23, 172, 13, 15), new Rectangle(42, 172, 13, 15)},
            {new Rectangle(4, 193, 13, 15), new Rectangle(23, 193, 13, 15), new Rectangle(42, 193, 13, 15)},
            {new Rectangle(4, 214, 13, 15), new Rectangle(23, 214, 13, 15), new Rectangle(42, 214, 13, 15)},
            {new Rectangle(4, 235, 13, 15), new Rectangle(23, 235, 13, 15), new Rectangle(42, 235, 13, 15)},
            {new Rectangle(4, 256, 13, 15), new Rectangle(23, 256, 13, 15), new Rectangle(42, 256, 13, 15)}};

        public UscXpertTippingBlokk()
        {
            InitializeComponent();
            data = new XpertTippingBlokk();
        }

        public XpertTippingBlokk Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Beskrivelse
        {
            get
            {
                return lblBeskrivelse.Text;
            }
            set
            {
                lblBeskrivelse.Text = value;
            }
        }

        private Region KalkulerOppdaterRegion(int nMarkering)
        {
            Region region = new Region();
            region.Union(pos[nMarkering, 0]);
            region.Union(pos[nMarkering, 1]);
            region.Union(pos[nMarkering, 2]);
            return region;
        }

        private void TegnMarkeringer(Graphics graphics, Image kryss, byte[] markeringer, Rectangle clipRectangle)
        {
            for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
            {
                switch (markeringer[nMarkering])
                {
                    case 1:
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 2:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        break;
                    case 3:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 4:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        break;
                    case 5:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 6:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        break;
                    case 7:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            TegnMarkeringer(e.Graphics, Properties.Resources.kryss, data.Markering, e.ClipRectangle);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            for (int nMarkering = 0; nMarkering < data.Markering.Length; nMarkering++)
            {
                if ((e.Y >= pos[nMarkering, 0].Y) && (e.Y < pos[nMarkering, 0].Y + (pos[nMarkering, 0].Height)))
                {
                    for (int nKolonne = 0; nKolonne < 3; nKolonne++)
                    {
                        if (pos[nMarkering, nKolonne].Contains(e.Location))
                        {
                            switch (nKolonne)
                            {
                                case 0: data.Markering[nMarkering] ^= 4;
                                    break;
                                case 1: data.Markering[nMarkering] ^= 2;
                                    break;
                                case 2: data.Markering[nMarkering] ^= 1;
                                    break;
                            }
                            Invalidate(pos[nMarkering, nKolonne]);
                            return;
                        }
                    }
                }
            }
        }
    }
}
