﻿namespace Xpert.Kontroller.Tipping
{
    partial class UscXpertTippingMaksRad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UscXpertTippingMaksRad));
            this.lblBeskrivelse = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBeskrivelse
            // 
            this.lblBeskrivelse.AutoEllipsis = true;
            this.lblBeskrivelse.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblBeskrivelse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBeskrivelse.Location = new System.Drawing.Point(0, 276);
            this.lblBeskrivelse.Name = "lblBeskrivelse";
            this.lblBeskrivelse.Size = new System.Drawing.Size(59, 13);
            this.lblBeskrivelse.TabIndex = 0;
            this.lblBeskrivelse.Text = "Maks på rad";
            this.lblBeskrivelse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UscTipperuMaksRad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.lblBeskrivelse);
            this.DoubleBuffered = true;
            this.Name = "UscTipperuMaksRad";
            this.Size = new System.Drawing.Size(59, 289);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblBeskrivelse;
    }
}
