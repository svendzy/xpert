﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingLaginfo : UserControl
    {
        private string[,] lag;
        private bool lastet = false;
        Rectangle[] pos = new Rectangle[4];

        public UscXpertTippingLaginfo()
        {
            InitializeComponent();
            lag = new string[12,2];
        }

        public string[,] Lag
        {
            set
            {
                lag = value;
                KalkulerPosisjoner();
                if (pos[3] == Rectangle.Empty)
                    this.Width = 40;
                else
                    this.Width = pos[3].X + pos[3].Width;
                if (lastet)
                    this.Update();
            }
        }

        private bool ErLagUtfylt
        {
            get
            {
                for (int n = 0; n < 12; n++)
                {
                    if (!String.IsNullOrEmpty(lag[n, 0]))
                        return true;
                    if (!String.IsNullOrEmpty(lag[n, 1]))
                        return true;
                }
                return false;
            }
        }

        private void KalkulerPosisjoner()
        {
            pos[0] = new Rectangle(0, 24, 30, 15);
            if (ErLagUtfylt)
            {
                pos[1] = new Rectangle(30, 24, 100, 15);
                pos[2] = new Rectangle(130, 24, 10, 15);
                pos[3] = new Rectangle(140, 24, 100, 15);
            }
            else
            {
                pos[1] = Rectangle.Empty;
                pos[2] = new Rectangle(30, 24, 10, 15);
                pos[3] = Rectangle.Empty;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lastet = true;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            using (StringFormat sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;
                
                Rectangle rect_0 = pos[0];
                Rectangle rect_1 = pos[1];
                Rectangle rect_2 = pos[2];
                Rectangle rect_3 = pos[3];

                for (int n = 0; n < 12; n++)
                {
                    e.Graphics.DrawString((n + 1).ToString() + ".", this.Font, Brushes.Black, (RectangleF)rect_0, sf);    
                    if (!rect_1.IsEmpty)
                        e.Graphics.DrawString(lag[n, 0], this.Font, Brushes.Black, (RectangleF)rect_1, sf);
                    if (!rect_3.IsEmpty)
                        e.Graphics.DrawString(lag[n, 1], this.Font, Brushes.Black, (RectangleF)rect_3, sf);
                    e.Graphics.DrawString("-", this.Font, Brushes.Black, (RectangleF)rect_2, sf);
                    
                    rect_0.Offset(0, 21);
                    rect_1.Offset(0, 21);
                    rect_2.Offset(0, 21);
                    rect_3.Offset(0, 21);
                }
            }
        }
    }
}
