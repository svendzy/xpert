﻿namespace Xpert
{
    partial class DlgXpertFotballLaginfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLagH1 = new System.Windows.Forms.TextBox();
            this.txtLagB1 = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lblHjemmelag = new System.Windows.Forms.Label();
            this.lblBortelag = new System.Windows.Forms.Label();
            this.sep1 = new System.Windows.Forms.Label();
            this.sep2 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.txtLagB2 = new System.Windows.Forms.TextBox();
            this.txtLagH2 = new System.Windows.Forms.TextBox();
            this.sep3 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.txtLagB3 = new System.Windows.Forms.TextBox();
            this.txtLagH3 = new System.Windows.Forms.TextBox();
            this.sep4 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.txtLagB4 = new System.Windows.Forms.TextBox();
            this.txtLagH4 = new System.Windows.Forms.TextBox();
            this.sep5 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.txtLagB5 = new System.Windows.Forms.TextBox();
            this.txtLagH5 = new System.Windows.Forms.TextBox();
            this.sep6 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.txtLagB6 = new System.Windows.Forms.TextBox();
            this.txtLagH6 = new System.Windows.Forms.TextBox();
            this.sep7 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.txtLagB7 = new System.Windows.Forms.TextBox();
            this.txtLagH7 = new System.Windows.Forms.TextBox();
            this.sep8 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.txtLagB8 = new System.Windows.Forms.TextBox();
            this.txtLagH8 = new System.Windows.Forms.TextBox();
            this.sep9 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.txtLagB9 = new System.Windows.Forms.TextBox();
            this.txtLagH9 = new System.Windows.Forms.TextBox();
            this.sep10 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.txtLagB10 = new System.Windows.Forms.TextBox();
            this.txtLagH10 = new System.Windows.Forms.TextBox();
            this.sep11 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.txtLagB11 = new System.Windows.Forms.TextBox();
            this.txtLagH11 = new System.Windows.Forms.TextBox();
            this.sep12 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.txtLagB12 = new System.Windows.Forms.TextBox();
            this.txtLagH12 = new System.Windows.Forms.TextBox();
            this.lbxLag = new System.Windows.Forms.ListBox();
            this.cbxLand = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLagH1
            // 
            this.txtLagH1.Location = new System.Drawing.Point(196, 33);
            this.txtLagH1.Name = "txtLagH1";
            this.txtLagH1.Size = new System.Drawing.Size(116, 20);
            this.txtLagH1.TabIndex = 2;
            this.txtLagH1.Enter += new System.EventHandler(this.txtLagH1_Enter);
            // 
            // txtLagB1
            // 
            this.txtLagB1.Location = new System.Drawing.Point(330, 33);
            this.txtLagB1.Name = "txtLagB1";
            this.txtLagB1.Size = new System.Drawing.Size(116, 20);
            this.txtLagB1.TabIndex = 3;
            this.txtLagB1.Enter += new System.EventHandler(this.txtLagB1_Enter);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(174, 36);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(16, 13);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "1.";
            // 
            // lblHjemmelag
            // 
            this.lblHjemmelag.AutoSize = true;
            this.lblHjemmelag.ForeColor = System.Drawing.Color.Blue;
            this.lblHjemmelag.Location = new System.Drawing.Point(196, 14);
            this.lblHjemmelag.Name = "lblHjemmelag";
            this.lblHjemmelag.Size = new System.Drawing.Size(59, 13);
            this.lblHjemmelag.TabIndex = 0;
            this.lblHjemmelag.Text = "Hjemmelag";
            // 
            // lblBortelag
            // 
            this.lblBortelag.AutoSize = true;
            this.lblBortelag.ForeColor = System.Drawing.Color.Blue;
            this.lblBortelag.Location = new System.Drawing.Point(330, 14);
            this.lblBortelag.Name = "lblBortelag";
            this.lblBortelag.Size = new System.Drawing.Size(46, 13);
            this.lblBortelag.TabIndex = 0;
            this.lblBortelag.Text = "Bortelag";
            // 
            // sep1
            // 
            this.sep1.AutoSize = true;
            this.sep1.Location = new System.Drawing.Point(316, 36);
            this.sep1.Name = "sep1";
            this.sep1.Size = new System.Drawing.Size(10, 13);
            this.sep1.TabIndex = 0;
            this.sep1.Text = "-";
            // 
            // sep2
            // 
            this.sep2.AutoSize = true;
            this.sep2.Location = new System.Drawing.Point(316, 62);
            this.sep2.Name = "sep2";
            this.sep2.Size = new System.Drawing.Size(10, 13);
            this.sep2.TabIndex = 0;
            this.sep2.Text = "-";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(174, 62);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(16, 13);
            this.lbl2.TabIndex = 0;
            this.lbl2.Text = "2.";
            // 
            // txtLagB2
            // 
            this.txtLagB2.Location = new System.Drawing.Point(330, 59);
            this.txtLagB2.Name = "txtLagB2";
            this.txtLagB2.Size = new System.Drawing.Size(116, 20);
            this.txtLagB2.TabIndex = 5;
            this.txtLagB2.Enter += new System.EventHandler(this.txtLagB2_Enter);
            // 
            // txtLagH2
            // 
            this.txtLagH2.Location = new System.Drawing.Point(196, 59);
            this.txtLagH2.Name = "txtLagH2";
            this.txtLagH2.Size = new System.Drawing.Size(116, 20);
            this.txtLagH2.TabIndex = 4;
            this.txtLagH2.Enter += new System.EventHandler(this.txtLagH2_Enter);
            // 
            // sep3
            // 
            this.sep3.AutoSize = true;
            this.sep3.Location = new System.Drawing.Point(316, 88);
            this.sep3.Name = "sep3";
            this.sep3.Size = new System.Drawing.Size(10, 13);
            this.sep3.TabIndex = 0;
            this.sep3.Text = "-";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(174, 88);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(16, 13);
            this.lbl3.TabIndex = 0;
            this.lbl3.Text = "3.";
            // 
            // txtLagB3
            // 
            this.txtLagB3.Location = new System.Drawing.Point(330, 85);
            this.txtLagB3.Name = "txtLagB3";
            this.txtLagB3.Size = new System.Drawing.Size(116, 20);
            this.txtLagB3.TabIndex = 7;
            this.txtLagB3.Enter += new System.EventHandler(this.txtLagB3_Enter);
            // 
            // txtLagH3
            // 
            this.txtLagH3.Location = new System.Drawing.Point(196, 85);
            this.txtLagH3.Name = "txtLagH3";
            this.txtLagH3.Size = new System.Drawing.Size(116, 20);
            this.txtLagH3.TabIndex = 6;
            this.txtLagH3.Enter += new System.EventHandler(this.txtLagH3_Enter);
            // 
            // sep4
            // 
            this.sep4.AutoSize = true;
            this.sep4.Location = new System.Drawing.Point(316, 114);
            this.sep4.Name = "sep4";
            this.sep4.Size = new System.Drawing.Size(10, 13);
            this.sep4.TabIndex = 0;
            this.sep4.Text = "-";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(174, 114);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(16, 13);
            this.lbl4.TabIndex = 0;
            this.lbl4.Text = "4.";
            // 
            // txtLagB4
            // 
            this.txtLagB4.Location = new System.Drawing.Point(330, 111);
            this.txtLagB4.Name = "txtLagB4";
            this.txtLagB4.Size = new System.Drawing.Size(116, 20);
            this.txtLagB4.TabIndex = 9;
            this.txtLagB4.Enter += new System.EventHandler(this.txtLagB4_Enter);
            // 
            // txtLagH4
            // 
            this.txtLagH4.Location = new System.Drawing.Point(196, 111);
            this.txtLagH4.Name = "txtLagH4";
            this.txtLagH4.Size = new System.Drawing.Size(116, 20);
            this.txtLagH4.TabIndex = 8;
            this.txtLagH4.Enter += new System.EventHandler(this.txtLagH4_Enter);
            // 
            // sep5
            // 
            this.sep5.AutoSize = true;
            this.sep5.Location = new System.Drawing.Point(316, 140);
            this.sep5.Name = "sep5";
            this.sep5.Size = new System.Drawing.Size(10, 13);
            this.sep5.TabIndex = 0;
            this.sep5.Text = "-";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(174, 140);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(16, 13);
            this.lbl5.TabIndex = 0;
            this.lbl5.Text = "5.";
            // 
            // txtLagB5
            // 
            this.txtLagB5.Location = new System.Drawing.Point(330, 137);
            this.txtLagB5.Name = "txtLagB5";
            this.txtLagB5.Size = new System.Drawing.Size(116, 20);
            this.txtLagB5.TabIndex = 11;
            this.txtLagB5.Enter += new System.EventHandler(this.txtLagB5_Enter);
            // 
            // txtLagH5
            // 
            this.txtLagH5.Location = new System.Drawing.Point(196, 137);
            this.txtLagH5.Name = "txtLagH5";
            this.txtLagH5.Size = new System.Drawing.Size(116, 20);
            this.txtLagH5.TabIndex = 10;
            this.txtLagH5.Enter += new System.EventHandler(this.txtLagH5_Enter);
            // 
            // sep6
            // 
            this.sep6.AutoSize = true;
            this.sep6.Location = new System.Drawing.Point(316, 166);
            this.sep6.Name = "sep6";
            this.sep6.Size = new System.Drawing.Size(10, 13);
            this.sep6.TabIndex = 0;
            this.sep6.Text = "-";
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Location = new System.Drawing.Point(174, 166);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(16, 13);
            this.lbl6.TabIndex = 0;
            this.lbl6.Text = "6.";
            // 
            // txtLagB6
            // 
            this.txtLagB6.Location = new System.Drawing.Point(330, 163);
            this.txtLagB6.Name = "txtLagB6";
            this.txtLagB6.Size = new System.Drawing.Size(116, 20);
            this.txtLagB6.TabIndex = 13;
            this.txtLagB6.Enter += new System.EventHandler(this.txtLagB6_Enter);
            // 
            // txtLagH6
            // 
            this.txtLagH6.Location = new System.Drawing.Point(196, 163);
            this.txtLagH6.Name = "txtLagH6";
            this.txtLagH6.Size = new System.Drawing.Size(116, 20);
            this.txtLagH6.TabIndex = 12;
            this.txtLagH6.Enter += new System.EventHandler(this.txtLagH6_Enter);
            // 
            // sep7
            // 
            this.sep7.AutoSize = true;
            this.sep7.Location = new System.Drawing.Point(316, 192);
            this.sep7.Name = "sep7";
            this.sep7.Size = new System.Drawing.Size(10, 13);
            this.sep7.TabIndex = 0;
            this.sep7.Text = "-";
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Location = new System.Drawing.Point(174, 192);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(16, 13);
            this.lbl7.TabIndex = 0;
            this.lbl7.Text = "7.";
            // 
            // txtLagB7
            // 
            this.txtLagB7.Location = new System.Drawing.Point(330, 189);
            this.txtLagB7.Name = "txtLagB7";
            this.txtLagB7.Size = new System.Drawing.Size(116, 20);
            this.txtLagB7.TabIndex = 15;
            this.txtLagB7.Enter += new System.EventHandler(this.txtLagB7_Enter);
            // 
            // txtLagH7
            // 
            this.txtLagH7.Location = new System.Drawing.Point(196, 189);
            this.txtLagH7.Name = "txtLagH7";
            this.txtLagH7.Size = new System.Drawing.Size(116, 20);
            this.txtLagH7.TabIndex = 14;
            this.txtLagH7.Enter += new System.EventHandler(this.txtLagH7_Enter);
            // 
            // sep8
            // 
            this.sep8.AutoSize = true;
            this.sep8.Location = new System.Drawing.Point(316, 218);
            this.sep8.Name = "sep8";
            this.sep8.Size = new System.Drawing.Size(10, 13);
            this.sep8.TabIndex = 0;
            this.sep8.Text = "-";
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Location = new System.Drawing.Point(174, 218);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(16, 13);
            this.lbl8.TabIndex = 0;
            this.lbl8.Text = "8.";
            // 
            // txtLagB8
            // 
            this.txtLagB8.Location = new System.Drawing.Point(330, 215);
            this.txtLagB8.Name = "txtLagB8";
            this.txtLagB8.Size = new System.Drawing.Size(116, 20);
            this.txtLagB8.TabIndex = 17;
            this.txtLagB8.Enter += new System.EventHandler(this.txtLagB8_Enter);
            // 
            // txtLagH8
            // 
            this.txtLagH8.Location = new System.Drawing.Point(196, 215);
            this.txtLagH8.Name = "txtLagH8";
            this.txtLagH8.Size = new System.Drawing.Size(116, 20);
            this.txtLagH8.TabIndex = 16;
            this.txtLagH8.Enter += new System.EventHandler(this.txtLagH8_Enter);
            // 
            // sep9
            // 
            this.sep9.AutoSize = true;
            this.sep9.Location = new System.Drawing.Point(316, 244);
            this.sep9.Name = "sep9";
            this.sep9.Size = new System.Drawing.Size(10, 13);
            this.sep9.TabIndex = 0;
            this.sep9.Text = "-";
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.Location = new System.Drawing.Point(174, 244);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(16, 13);
            this.lbl9.TabIndex = 0;
            this.lbl9.Text = "9.";
            // 
            // txtLagB9
            // 
            this.txtLagB9.Location = new System.Drawing.Point(330, 241);
            this.txtLagB9.Name = "txtLagB9";
            this.txtLagB9.Size = new System.Drawing.Size(116, 20);
            this.txtLagB9.TabIndex = 19;
            this.txtLagB9.Enter += new System.EventHandler(this.txtLagB9_Enter);
            // 
            // txtLagH9
            // 
            this.txtLagH9.Location = new System.Drawing.Point(196, 241);
            this.txtLagH9.Name = "txtLagH9";
            this.txtLagH9.Size = new System.Drawing.Size(116, 20);
            this.txtLagH9.TabIndex = 18;
            this.txtLagH9.Enter += new System.EventHandler(this.txtLagH9_Enter);
            // 
            // sep10
            // 
            this.sep10.AutoSize = true;
            this.sep10.Location = new System.Drawing.Point(316, 270);
            this.sep10.Name = "sep10";
            this.sep10.Size = new System.Drawing.Size(10, 13);
            this.sep10.TabIndex = 0;
            this.sep10.Text = "-";
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(168, 270);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(22, 13);
            this.lbl10.TabIndex = 0;
            this.lbl10.Text = "10.";
            // 
            // txtLagB10
            // 
            this.txtLagB10.Location = new System.Drawing.Point(330, 267);
            this.txtLagB10.Name = "txtLagB10";
            this.txtLagB10.Size = new System.Drawing.Size(116, 20);
            this.txtLagB10.TabIndex = 21;
            this.txtLagB10.Enter += new System.EventHandler(this.txtLagB10_Enter);
            // 
            // txtLagH10
            // 
            this.txtLagH10.Location = new System.Drawing.Point(196, 267);
            this.txtLagH10.Name = "txtLagH10";
            this.txtLagH10.Size = new System.Drawing.Size(116, 20);
            this.txtLagH10.TabIndex = 20;
            this.txtLagH10.Enter += new System.EventHandler(this.txtLagH10_Enter);
            // 
            // sep11
            // 
            this.sep11.AutoSize = true;
            this.sep11.Location = new System.Drawing.Point(316, 296);
            this.sep11.Name = "sep11";
            this.sep11.Size = new System.Drawing.Size(10, 13);
            this.sep11.TabIndex = 0;
            this.sep11.Text = "-";
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.Location = new System.Drawing.Point(168, 296);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(22, 13);
            this.lbl11.TabIndex = 0;
            this.lbl11.Text = "11.";
            // 
            // txtLagB11
            // 
            this.txtLagB11.Location = new System.Drawing.Point(330, 293);
            this.txtLagB11.Name = "txtLagB11";
            this.txtLagB11.Size = new System.Drawing.Size(116, 20);
            this.txtLagB11.TabIndex = 23;
            this.txtLagB11.Enter += new System.EventHandler(this.txtLagB11_Enter);
            // 
            // txtLagH11
            // 
            this.txtLagH11.Location = new System.Drawing.Point(196, 293);
            this.txtLagH11.Name = "txtLagH11";
            this.txtLagH11.Size = new System.Drawing.Size(116, 20);
            this.txtLagH11.TabIndex = 22;
            this.txtLagH11.Enter += new System.EventHandler(this.txtLagH11_Enter);
            // 
            // sep12
            // 
            this.sep12.AutoSize = true;
            this.sep12.Location = new System.Drawing.Point(316, 322);
            this.sep12.Name = "sep12";
            this.sep12.Size = new System.Drawing.Size(10, 13);
            this.sep12.TabIndex = 0;
            this.sep12.Text = "-";
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.Location = new System.Drawing.Point(168, 322);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(22, 13);
            this.lbl12.TabIndex = 0;
            this.lbl12.Text = "12.";
            // 
            // txtLagB12
            // 
            this.txtLagB12.Location = new System.Drawing.Point(330, 319);
            this.txtLagB12.Name = "txtLagB12";
            this.txtLagB12.Size = new System.Drawing.Size(116, 20);
            this.txtLagB12.TabIndex = 25;
            this.txtLagB12.Enter += new System.EventHandler(this.txtLagB12_Enter);
            // 
            // txtLagH12
            // 
            this.txtLagH12.Location = new System.Drawing.Point(196, 319);
            this.txtLagH12.Name = "txtLagH12";
            this.txtLagH12.Size = new System.Drawing.Size(116, 20);
            this.txtLagH12.TabIndex = 24;
            this.txtLagH12.Enter += new System.EventHandler(this.txtLagH12_Enter);
            // 
            // lbxLag
            // 
            this.lbxLag.FormattingEnabled = true;
            this.lbxLag.Location = new System.Drawing.Point(13, 62);
            this.lbxLag.Name = "lbxLag";
            this.lbxLag.Size = new System.Drawing.Size(149, 303);
            this.lbxLag.Sorted = true;
            this.lbxLag.TabIndex = 1;
            this.lbxLag.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbxLag_MouseDoubleClick);
            // 
            // cbxLand
            // 
            this.cbxLand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxLand.FormattingEnabled = true;
            this.cbxLand.Location = new System.Drawing.Point(13, 33);
            this.cbxLand.Name = "cbxLand";
            this.cbxLand.Size = new System.Drawing.Size(149, 21);
            this.cbxLand.TabIndex = 0;
            this.cbxLand.SelectedIndexChanged += new System.EventHandler(this.cbxLand_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Blue;
            this.label27.Location = new System.Drawing.Point(12, 14);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Land";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(371, 348);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // DlgXpertFotballLaginfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 383);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.cbxLand);
            this.Controls.Add(this.lbxLag);
            this.Controls.Add(this.sep12);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.txtLagB12);
            this.Controls.Add(this.txtLagH12);
            this.Controls.Add(this.sep11);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.txtLagB11);
            this.Controls.Add(this.txtLagH11);
            this.Controls.Add(this.sep10);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.txtLagB10);
            this.Controls.Add(this.txtLagH10);
            this.Controls.Add(this.sep9);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.txtLagB9);
            this.Controls.Add(this.txtLagH9);
            this.Controls.Add(this.sep8);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.txtLagB8);
            this.Controls.Add(this.txtLagH8);
            this.Controls.Add(this.sep7);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.txtLagB7);
            this.Controls.Add(this.txtLagH7);
            this.Controls.Add(this.sep6);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.txtLagB6);
            this.Controls.Add(this.txtLagH6);
            this.Controls.Add(this.sep5);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.txtLagB5);
            this.Controls.Add(this.txtLagH5);
            this.Controls.Add(this.sep4);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.txtLagB4);
            this.Controls.Add(this.txtLagH4);
            this.Controls.Add(this.sep3);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.txtLagB3);
            this.Controls.Add(this.txtLagH3);
            this.Controls.Add(this.sep2);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.txtLagB2);
            this.Controls.Add(this.txtLagH2);
            this.Controls.Add(this.sep1);
            this.Controls.Add(this.lblBortelag);
            this.Controls.Add(this.lblHjemmelag);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.txtLagB1);
            this.Controls.Add(this.txtLagH1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgXpertFotballLaginfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Kampoppsett";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLagH1;
        private System.Windows.Forms.TextBox txtLagB1;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lblHjemmelag;
        private System.Windows.Forms.Label lblBortelag;
        private System.Windows.Forms.Label sep1;
        private System.Windows.Forms.Label sep2;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.TextBox txtLagB2;
        private System.Windows.Forms.TextBox txtLagH2;
        private System.Windows.Forms.Label sep3;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.TextBox txtLagB3;
        private System.Windows.Forms.TextBox txtLagH3;
        private System.Windows.Forms.Label sep4;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.TextBox txtLagB4;
        private System.Windows.Forms.TextBox txtLagH4;
        private System.Windows.Forms.Label sep5;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.TextBox txtLagB5;
        private System.Windows.Forms.TextBox txtLagH5;
        private System.Windows.Forms.Label sep6;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.TextBox txtLagB6;
        private System.Windows.Forms.TextBox txtLagH6;
        private System.Windows.Forms.Label sep7;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.TextBox txtLagB7;
        private System.Windows.Forms.TextBox txtLagH7;
        private System.Windows.Forms.Label sep8;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.TextBox txtLagB8;
        private System.Windows.Forms.TextBox txtLagH8;
        private System.Windows.Forms.Label sep9;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.TextBox txtLagB9;
        private System.Windows.Forms.TextBox txtLagH9;
        private System.Windows.Forms.Label sep10;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.TextBox txtLagB10;
        private System.Windows.Forms.TextBox txtLagH10;
        private System.Windows.Forms.Label sep11;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.TextBox txtLagB11;
        private System.Windows.Forms.TextBox txtLagH11;
        private System.Windows.Forms.Label sep12;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.TextBox txtLagB12;
        private System.Windows.Forms.TextBox txtLagH12;
        private System.Windows.Forms.ListBox lbxLag;
        private System.Windows.Forms.ComboBox cbxLand;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnOK;
    }
}