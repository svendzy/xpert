﻿namespace Xpert
{
    partial class DlgXpertFotballPremieKontroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgXpertFotballPremieKontroll));
            this.btnKontroller = new System.Windows.Forms.Button();
            this.tippingResultatIndikator = new Xpert.Kontroller.Tipping.XpertTippingPremieIndikator();
            this.uscTippingResultat = new Xpert.Kontroller.Tipping.UscXpertTippingResultat();
            this.SuspendLayout();
            // 
            // btnKontroller
            // 
            this.btnKontroller.Location = new System.Drawing.Point(12, 12);
            this.btnKontroller.Name = "btnKontroller";
            this.btnKontroller.Size = new System.Drawing.Size(75, 23);
            this.btnKontroller.TabIndex = 4;
            this.btnKontroller.Text = "Kontroller";
            this.btnKontroller.UseVisualStyleBackColor = true;
            this.btnKontroller.Click += new System.EventHandler(this.btnKontroller_Click);
            // 
            // tippingResultatIndikator
            // 
            this.tippingResultatIndikator.Location = new System.Drawing.Point(100, 78);
            this.tippingResultatIndikator.Name = "tippingResultatIndikator";
            this.tippingResultatIndikator.Size = new System.Drawing.Size(330, 250);
            this.tippingResultatIndikator.TabIndex = 3;
            this.tippingResultatIndikator.Text = "tipperuIndikator1";
            // 
            // uscTippingResultat
            // 
            this.uscTippingResultat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uscTippingResultat.BackgroundImage")));
            this.uscTippingResultat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.uscTippingResultat.Location = new System.Drawing.Point(12, 54);
            this.uscTippingResultat.Name = "uscTippingResultat";
            this.uscTippingResultat.Size = new System.Drawing.Size(59, 289);
            this.uscTippingResultat.Stamme = null;
            this.uscTippingResultat.TabIndex = 2;
            // 
            // DlgXpertFotballPremieKontroll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 350);
            this.Controls.Add(this.btnKontroller);
            this.Controls.Add(this.tippingResultatIndikator);
            this.Controls.Add(this.uscTippingResultat);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgXpertFotballPremieKontroll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Premiekontroll";
            this.ResumeLayout(false);

        }

        #endregion

        private Xpert.Kontroller.Tipping.UscXpertTippingResultat uscTippingResultat;
        private Xpert.Kontroller.Tipping.XpertTippingPremieIndikator tippingResultatIndikator;
        private System.Windows.Forms.Button btnKontroller;
    }
}